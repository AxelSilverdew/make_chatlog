use clap::Parser;
use std::fs::create_dir_all;
use std::path::Path;
use chrono;


/// I use this program to generate folders to store chatlogs
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Name of the batch
    #[clap(short, long)]
    batch: String,

    /// Module number
    #[clap(short, long, default_value = "")]
    module: String,

    /// Directory that stores your chatlogs
    #[clap(short, long, default_value = "/home/abhiram/Documents/chatlogs")]
    chatlog_dir: String

}

fn main() {
    let args = Args::parse();
    let current_timestamp = chrono::offset::Local::now().format("%Y-%b-%d").to_string();
    let chat_file = format!("{}_{}.txt", args.batch, current_timestamp);
    let chat_dir = Path::new(&args.chatlog_dir).join(&args.batch).join(&args.module);
    let chat_file = chat_dir.join(&chat_file);
    if !Path::exists(&chat_dir) {
        create_dir_all(&chat_dir).unwrap_or_else(|why| {
            println!("! {:?}", why.kind());
        });
    }

    // let output = Command::new("nvim").arg(&chat_file).spawn().expect("Failed to run nvim.");
   subprocess::Exec::cmd("nvim").arg(&chat_file).join().unwrap(); 
}
