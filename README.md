# Make Chatlog

# Okay, Why?

I do a **_lot_** of teaching and it's really important that I save my chatlogs.

I used to manage these by hand but at the volume I'm at, it's insanely annoying to do that.

Hence, this smol program to help me out.

I don't intend for other people to actually use this, there's a lot of hardcoded stuff in here.

# Install

```bash
$ git clone git@gitlab.com:AxelSilverdew/make_chatlog.git
$ cd make_chatlog
$ cargo build --release
$ cp target/release/make-chatlog ~/.local/bin/make-chatlog
```

# Usage

```
make_chatlog 0.1.0
I use this program to generate folders to store chatlogs

USAGE:
    make-chatlog [OPTIONS] --batch <BATCH>

OPTIONS:
    -b, --batch <BATCH>                Name of the batch
    -c, --chatlog-dir <CHATLOG_DIR>    Directory that stores your chatlogs [default:
                                       /home/abhiram/Documents/chatlogs]
    -h, --help                         Print help information
    -m, --module <MODULE>              Module number [default: ]
    -V, --version                      Print version information
```
